<?php

//TODO -> this CORS could be dangerous
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Content-Type: application/json');

require_once('password.class.php'); 
require_once('conn.php'); 

if (!isset($_POST['usuario']) || !isset($_POST['pass'])){
	echo json_encode(0);
	die();
}
$usuario=$_POST['usuario']; 
$pass=$_POST['pass']; 
$usuario = stripslashes($usuario);
$usuario = mysqli_real_escape_string($mysqli, $usuario);
$pass = mysqli_real_escape_string($mysqli, $pass);

if ($result = mysqli_query($mysqli, "SELECT idUsuarios, status, usuario, pass_hashed, nombres, apellido_pat, apellido_mat, fecha_nac, email FROM usuarios WHERE usuario = '".$usuario."'")) {
	$row_cnt = mysqli_num_rows($result);
    if($row_cnt==1){
    	$row = $result->fetch_assoc();
    	if (Password::validatePassword($pass, $row['pass_hashed']) === false){
    		echo json_encode(0);
    	} else {
    		//sobreescribir el pass para que no se guarde en la app (por seguridad)
    		//esto es solo para permitir consumir los servicios durante la sesión
    		$row['pass_hashed'] = md5($row['nombres'].$row['fecha_nac']);
    		$row['login_success'] = true;
    		$_SESSION['userdata'] = $row;
    		echo json_encode($row);
    	}    	
    } else {
    	echo json_encode(0);
    }
}


?>