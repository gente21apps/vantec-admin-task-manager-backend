<?php

require_once('password.class.php'); //funciones hash, validate, etc para base de datos #2
require_once('connection.php'); // conexión ambas bases de datos

$usuario=$_POST['usuario']; 
$pass=$_POST['pass']; 
$usuario = stripslashes($usuario);
$usuario = mysqli_real_escape_string($con, $usuario);
$pass = mysqli_real_escape_string($con, $pass);

//query a la base de datos #1
if ($result = mysqli_query($con, "SELECT idUsuarios, status, usuario, pass, nombres, fecha_creacion FROM usuarios WHERE usuario = '".$usuario."' AND pass = '".$pass."'")) {

    $row_cnt = mysqli_num_rows($result);

    if($row_cnt==1){  //es admin -> puede usar la app

        echo 'Password is valid!'; //success, entra como "admin"

    } else { //es usuario -> le llegan notificaciones

        //query a la base de datos #2
        if ($result = mysqli_query($con2, "SELECT id, username, password, firstname FROM users WHERE username = '".$usuario."'")) {

            $row_cnt = mysqli_num_rows($result);
            if($row_cnt==1){
                $row = $result->fetch_assoc();
                if (Password::validatePassword($pass, $row['password']) === false){ //aquí utiliza validatePassword() de la clase Password
                    echo "Wrong Username or Password";
                } else {
                    echo 'Password is valid!'; //success, entra como "usuario normal"
                }
            } else {
                echo "Wrong Username or Password";
            }

        } else {

            echo "Wrong Username or Password";

        }

    }
    mysqli_free_result($result);
} else {
    echo "Wrong Username or Password";
}
?>